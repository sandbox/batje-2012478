api = 2
core = 7.x

projects[admin_menu][subdir] = contrib
projects[admin_menu][type] = "module"
projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][patch][] = https://www.drupal.org/files/issues/admin_menu-fix-js-error-with-new-jquery-2326147-2.patch

projects[cer][subdir] = contrib
projects[cer][type] = "module"
projects[cer][version] = "3.0-alpha7"

projects[charts_graphs][subdir] = contrib
projects[charts_graphs][type] = "module"
projects[charts_graphs][version] = "2.x-dev"

projects[charts_graphs_flot][subdir] = contrib
projects[charts_graphs_flot][type] = "module"
projects[charts_graphs_flot][version] = "1.x-dev"
projects[charts_graphs_flot][patch][] = "https://www.drupal.org/files/issues/charts_graphs_flot-fixes-getChart-warning-at-line-232-2324399-0.patch"
projects[charts_graphs_flot][patch][] = "https://www.drupal.org/files/issues/charts_graphs_flot-fix-for-xaxis-warning-2324451-2.patch"

projects[context][subdir] = contrib
projects[context][type] = "module"
projects[context][version] = "3.2"

projects[ctools][subdir] = contrib
projects[ctools][type] = "module"
projects[ctools][version] = "1.4"

projects[date][subdir] = contrib
projects[date][type] = "module"
projects[date][version] = "2.8"

projects[date_facets][subdir] = contrib
projects[date_facets][type] = "module"
projects[date_facets][version] = "1.x-dev"

projects[entity][subdir] = contrib
projects[entity][type] = "module"
projects[entity][version] = "1.5"

projects[entityreference][subdir] = contrib
projects[entityreference][type] = "module"
projects[entityreference][version] = "1.1"

projects[facetapi][subdir] = contrib
projects[facetapi][type] = "module"
projects[facetapi][version] = "1.5"

projects[facetapi_graphs][subdir] = contrib
projects[facetapi_graphs][type] = "module"
projects[facetapi_graphs][version] = "1.x-dev"

projects[facetapi_realms][subdir] = contrib
projects[facetapi_realms][type] = "module"
projects[facetapi_realms][download][type] = git
projects[facetapi_realms][download][url] = "http://git.drupal.org/sandbox/batje/1570344.git"
projects[facetapi_realms][download][branch] = master

projects[facetapi_tagcloud][subdir] = contrib
projects[facetapi_tagcloud][type] = "module"
projects[facetapi_tagcloud][version] = 1.0-beta1
projects[facetapi_tagcloud][patch][] = "https://www.drupal.org/files/issues/2311599-%235_Count_showing_0_0.patch"

projects[features][subdir] = contrib
projects[features][type] = "module"
projects[features][version] = "2.2"

projects[flot][subdir] = contrib
projects[flot][type] = "module"
projects[flot][version] = "1.x-dev"
projects[flot][patch][] = "https://www.drupal.org/files/issues/flot-default_objects-2264691-4.patch"

projects[field_collection][subdir] = contrib
projects[field_collection][type] = "module"
projects[field_collection][version] = "1.0-beta7"

projects[field_collection_deploy][subdir] = contrib
projects[field_collection_deploy][type] = "module"
projects[field_collection_deploy][version] = "1.0-beta3"

projects[field_extrawidgets][subdir] = contrib
projects[field_extrawidgets][type] = "module"
projects[field_extrawidgets][version] = "1.1"

projects[libraries][subdir] = contrib
projects[libraries][type] = "module"
projects[libraries][version] = "2.2"

projects[purl][subdir] = contrib
projects[purl][type] = "module"
projects[purl][version] = "1.x-dev"

projects[quicktabs][subdir] = contrib
projects[quicktabs][type] = "module"
projects[quicktabs][version] = "3.6"

projects[module_filter][subdir] = contrib
projects[module_filter][type] = "module"
projects[module_filter][version] = "2.0-alpha2"

projects[node_export][subdir] = contrib
projects[node_export][type] = "module"
projects[node_export][version] = "3.0"

projects[rules][subdir] = contrib
projects[rules][type] = "module"
projects[rules][version] = "2.7"

projects[token][subdir] = contrib
projects[token][type] = "module"
projects[token][version] = "1.5"

projects[search_api][subdir] = contrib
projects[search_api][type] = "module"
projects[search_api][version] = "1.13"

projects[search_api_db][subdir] = contrib
projects[search_api_db][type] = "module"
projects[search_api_db][version] = "1.4"

projects[search_api_solr][subdir] = contrib
projects[search_api_solr][type] = "module"
projects[search_api_solr][version] = "1.5"

projects[search_api_saved_searches][subdir] = contrib
projects[search_api_saved_searches][type] = "module"
projects[search_api_saved_searches][version] = "1.3"

projects[strongarm][subdir] = contrib
projects[strongarm][type] = "module"
projects[strongarm][version] = "2.0"

projects[uuid][subdir] = contrib
projects[uuid][type] = "module"
projects[uuid][version] = "1.0-alpha5"

projects[views][subdir] = contrib
projects[views][type] = "module"
projects[views][version] = "3.8"

projects[views_modes][subdir] = contrib
projects[views_modes][type] = module
projects[views_modes[version] = "1.x-dev"

projects[questionnaire][subdir] = patched
projects[questionnaire][type] = "module"
projects[questionnaire][download][type] = "git"
projects[questionnaire][download][branch] = "7.x-1.x"
projects[questionnaire][download][url]  = "http://git.drupal.org/project/questionnaire.git"

projects[smsframework][subdir] = patched
projects[smsframework][type] = "module"
projects[smsframework][download][type] = git
projects[smsframework][download][url] = "http://git.drupal.org/project/smsframework.git"
projects[smsframework][download][branch] = "7.x-1.x"
projects[smsframework][patch][] = "https://www.drupal.org/files/issues/smsframework-fix_for_sms_author_and_recipient_for_incoming_messages-2372029-1.patch"

projects[smsframework_tools][subdir] = patched
projects[smsframework_tools][type] = "module"
projects[smsframework_tools][download][type] = git
projects[smsframework_tools][download][url] = "http://git.drupal.org/sandbox/dmulindwa/2024955.git"
projects[smsframework_tools][download][branch] = "7.x-1.x"

libraries[flot][download][type] = "file"
libraries[flot][download][url] = "http://flot.googlecode.com/files/flot-0.7.tar.gz"
libraries[flot][download][sha1] = "68ca8b250d18203ebe67136913e0e1c82bbeecfb"
libraries[flot][destination] = "libraries"

# User management

projects[auto_username][subdir] = contrib
projects[auto_username][type] = "module"
projects[auto_username][version] = "1.0-alpha1"

projects[genpass][subdir] = contrib
projects[genpass][type] = "module"
projects[genpass][version] = "1"

# note that the module is called optional_mail_on_register
projects[optional_mail][subdir] = contrib
projects[optional_mail][type] = "module"
projects[optional_mail][version] = "1.1"

# Theme 

projects[bootstrap][subdir] = contrib
projects[bootstrap][type] = "theme"
projects[bootstrap][version] = "3"

projects[views_bootstrap][subdir] = contrib
projects[views_bootstrap][type] = "module"
projects[views_bootstrap][version] = "3.1"

projects[jquery_update][subdir] = contrib
projects[jquery_update][type] = "module"
projects[jquery_update][version] = "2.4"

projects[collapsiblock][subdir] = contrib
projects[collapsiblock][type] = "module"
projects[collapsiblock][version] = "1"

projects[jquery_ajax_load][subdir] = contrib
projects[jquery_ajax_load][type] = "module"
projects[jquery_ajax_load][version] = "1.4"

projects[twitter_bootstrap_modal][subdir] = contrib
projects[twitter_bootstrap_modal][type] = "module"
projects[twitter_bootstrap_modal][version] = "3.x-dev"
projects[twitter_bootstrap_modal][patch][] = https://www.drupal.org/files/issues/233879-%231-add-header-title-form-buttons.patch
