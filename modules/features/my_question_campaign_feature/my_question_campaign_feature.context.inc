<?php
/**
 * @file
 * my_question_campaign_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function my_question_campaign_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'my_question_create_campaign_context';
  $context->description = '';
  $context->tag = 'Questionnaire';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'my_question_user_campaign_segment' => 'my_question_user_campaign_segment',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'search_api_saved_searches-my_question_users' => array(
          'module' => 'search_api_saved_searches',
          'delta' => 'my_question_users',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'facetapi-2JfNqWUZwGKXQJ1Ju8fsNA8xcThQbsig' => array(
          'module' => 'facetapi',
          'delta' => '2JfNqWUZwGKXQJ1Ju8fsNA8xcThQbsig',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Questionnaire');
  $export['my_question_create_campaign_context'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'my_question_questionnaire_block_context';
  $context->description = 'Shows the Questionnaire Block on the user and homepage';
  $context->tag = 'Questionnaire';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
        'user' => 'user',
        'user/*' => 'user/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'questionnaire_block-question' => array(
          'module' => 'questionnaire_block',
          'delta' => 'question',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Questionnaire');
  t('Shows the Questionnaire Block on the user and homepage');
  $export['my_question_questionnaire_block_context'] = $context;

  return $export;
}
