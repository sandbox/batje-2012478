<?php
/**
 * @file
 * my_question_campaign_feature.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function my_question_campaign_feature_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_questionnaires'
  $field_bases['field_questionnaires'] = array(
    'active' => 1,
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_questionnaires',
    'foreign keys' => array(),
    'indexes' => array(
      'context' => array(
        0 => 'context_type',
        1 => 'context_id',
      ),
      'id' => array(
        0 => 'id',
      ),
    ),
    'locked' => 0,
    'module' => 'questionnaire_field',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'questionnaire_field',
  );

  return $field_bases;
}
