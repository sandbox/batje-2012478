<?php
/**
 * @file
 * my_question_campaign_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function my_question_campaign_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-field_questionnaires'
  $field_instances['user-user-field_questionnaires'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'questionnaire_field',
        'settings' => array(),
        'type' => 'questionnaire_field_link',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_questionnaires',
    'label' => 'Questionnaires',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'field_extrawidgets',
      'settings' => array(),
      'type' => 'field_extrawidgets_hidden',
      'weight' => 7,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Questionnaires');

  return $field_instances;
}
