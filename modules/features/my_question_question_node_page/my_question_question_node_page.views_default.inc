<?php
/**
 * @file
 * my_question_question_node_page.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function my_question_question_node_page_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'questionnaire_question_blocks';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'questionnaire_answer';
  $view->human_name = 'Questionnaire Question Blocks';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Graph';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Answer: Answer Text */
  $handler->display->display_options['fields']['answertext']['id'] = 'answertext';
  $handler->display->display_options['fields']['answertext']['table'] = 'questionnaire_answer';
  $handler->display->display_options['fields']['answertext']['field'] = 'answertext';
  $handler->display->display_options['fields']['answertext']['label'] = '';
  $handler->display->display_options['fields']['answertext']['element_label_colon'] = FALSE;
  /* Contextual filter: Answer: Question */
  $handler->display->display_options['arguments']['question']['id'] = 'question';
  $handler->display->display_options['arguments']['question']['table'] = 'questionnaire_answer';
  $handler->display->display_options['arguments']['question']['field'] = 'question';
  $handler->display->display_options['arguments']['question']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['question']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['question']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['question']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['question']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['question']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['question']['validate_options']['types'] = array(
    'questionnaire_question' => 'questionnaire_question',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');

  /* Display: Count */
  $handler = $view->new_display('block', 'Count', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: COUNT(DISTINCT Answer: Answer ID) */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'questionnaire_answer';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['group_type'] = 'count_distinct';
  $handler->display->display_options['fields']['id']['label'] = 'Responses';
  $handler->display->display_options['fields']['id']['separator'] = ' ';

  /* Display: Text Bar */
  $handler = $view->new_display('block', 'Text Bar', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Graph';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'charts';
  $handler->display->display_options['style_options']['height'] = '300';
  $handler->display->display_options['style_options']['type'] = array(
    'bluff' => 'line',
    'flot' => 'sidebarstack',
  );
  $handler->display->display_options['style_options']['showlegend'] = 0;
  $handler->display->display_options['style_options']['zoom'] = 0;
  $handler->display->display_options['style_options']['views_charts_series_fields'] = array(
    'id' => 'id',
  );
  $handler->display->display_options['style_options']['views_charts_x_labels'] = 'answertext';
  $handler->display->display_options['style_options']['engine'] = 'flot';
  $handler->display->display_options['style_options']['y_min'] = '';
  $handler->display->display_options['style_options']['y_legend'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Answer: Answer Text */
  $handler->display->display_options['fields']['answertext']['id'] = 'answertext';
  $handler->display->display_options['fields']['answertext']['table'] = 'questionnaire_answer';
  $handler->display->display_options['fields']['answertext']['field'] = 'answertext';
  $handler->display->display_options['fields']['answertext']['label'] = '';
  $handler->display->display_options['fields']['answertext']['element_label_colon'] = FALSE;
  /* Field: COUNT(DISTINCT Answer: Answer ID) */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'questionnaire_answer';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['group_type'] = 'count_distinct';
  $handler->display->display_options['fields']['id']['separator'] = '';

  /* Display: Interesting Answers List */
  $handler = $view->new_display('block', 'Interesting Answers List', 'block_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Highlights';
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Flags: questionnaire_answer_interesting */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'questionnaire_answer';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'Interesting Flag';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'questionnaire_answer_interesting';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  $export['questionnaire_question_blocks'] = $view;

  return $export;
}
