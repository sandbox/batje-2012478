<?php
/**
 * @file
 * my_question_question_node_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function my_question_question_node_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function my_question_question_node_page_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
