<?php
/**
 * @file
 * my_question_question_node_page.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function my_question_question_node_page_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'my_question_question_node_page';
  $context->description = 'My Question Question Node Page';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'questionnaire_question' => 'questionnaire_question',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-6744a74cd0fb3fdb06fbfb831439b0c3' => array(
          'module' => 'views',
          'delta' => '6744a74cd0fb3fdb06fbfb831439b0c3',
          'region' => 'content',
          'weight' => '-9',
        ),
        'facetapi-PwHXI5RpQYYzl3G6l0579GTPZrZ6l5cU' => array(
          'module' => 'facetapi',
          'delta' => 'PwHXI5RpQYYzl3G6l0579GTPZrZ6l5cU',
          'region' => 'content',
          'weight' => '-8',
        ),
        'views-7132ae1c11ea51d3dfeba4ebf652b21e' => array(
          'module' => 'views',
          'delta' => '7132ae1c11ea51d3dfeba4ebf652b21e',
          'region' => 'content',
          'weight' => '-7',
        ),
        'views-de973c5281a34f1e5e875341f7ec6738' => array(
          'module' => 'views',
          'delta' => 'de973c5281a34f1e5e875341f7ec6738',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
    'search_api' => array(
      'search_api_index' => 'answers',
      'facet' => 'question',
      'arg' => '1',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('My Question Question Node Page');
  $export['my_question_question_node_page'] = $context;

  return $export;
}
