<?php
/**
 * @file
 * devtrac7_solr_search_questions_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function devtrac7_solr_search_questions_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'questions';
  $context->description = '';
  $context->tag = 'answers&questions';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'devtrac_solr_view_questions' => 'devtrac_solr_view_questions',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-91',
        ),
        'facetapi-YtTGmzsTed723wjNeg6EbRh6pzk1SHJP' => array(
          'module' => 'facetapi',
          'delta' => 'YtTGmzsTed723wjNeg6EbRh6pzk1SHJP',
          'region' => 'sidebar_first',
          'weight' => '-91',
        ),
        'current_search-devtrac7_questions_standard' => array(
          'module' => 'current_search',
          'delta' => 'devtrac7_questions_standard',
          'region' => 'sidebar_first',
          'weight' => '-91',
        ),
        'facetapi-t7wLmJF0YmskVmorkmYOShGaHRkWzkRC' => array(
          'module' => 'facetapi',
          'delta' => 't7wLmJF0YmskVmorkmYOShGaHRkWzkRC',
          'region' => 'categories',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'informationalpage_layout',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('answers&questions');
  $export['questions'] = $context;

  return $export;
}
