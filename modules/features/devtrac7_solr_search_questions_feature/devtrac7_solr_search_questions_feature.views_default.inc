<?php
/**
 * @file
 * devtrac7_solr_search_questions_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function devtrac7_solr_search_questions_feature_views_default_views() {
  $views = array();

  // Exported view: devtrac_solr_view_questions
  $view = new view;
  $view->name = 'devtrac_solr_view_questions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_questions';
  $view->human_name = 'devtrac_solr_view_questions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Statistics';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'taxonomy_vocabulary_1',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
    1 => array(
      'field' => 'field_question_subject',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = '2';
  $handler->display->display_options['style_options']['fill_single_line'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['id'] = 'taxonomy_vocabulary_1';
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['table'] = 'search_api_index_questions';
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['field'] = 'taxonomy_vocabulary_1';
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['label'] = 'Questions on';
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['hide_alter_empty'] = 1;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_questions';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nid']['hide_alter_empty'] = 1;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_questions';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'questions/answers?f[0]=question:[nid]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;

  /* Display: List */
  $handler = $view->new_display('page', 'List', 'stats');
  $handler->display->display_options['display_description'] = 'Default view for questions on the main menu';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['field_question_subject']['id'] = 'field_question_subject';
  $handler->display->display_options['fields']['field_question_subject']['table'] = 'search_api_index_questions';
  $handler->display->display_options['fields']['field_question_subject']['field'] = 'field_question_subject';
  $handler->display->display_options['fields']['field_question_subject']['label'] = '';
  $handler->display->display_options['fields']['field_question_subject']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_question_subject']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_question_subject']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_question_subject']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_question_subject']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_question_subject']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_question_subject']['hide_alter_empty'] = 1;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['id'] = 'taxonomy_vocabulary_1';
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['table'] = 'search_api_index_questions';
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['field'] = 'taxonomy_vocabulary_1';
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['label'] = 'Statistics on';
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['taxonomy_vocabulary_1']['hide_alter_empty'] = 1;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_questions';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nid']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nid']['hide_alter_empty'] = 1;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_questions';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'statistics/answers?f[0]=questionnaire_answer_question:[nid]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['path'] = 'statistics';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Statistics';
  $handler->display->display_options['menu']['description'] = 'Statistics';
  $handler->display->display_options['menu']['weight'] = '20';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;

  $views[$view->name] = $view;

  return $views;
}
