<?php
/**
 * @file
 * devtrac7_solr_search_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function devtrac7_solr_search_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'solr_all_pages';
  $context->description = 'Active on all pages,';
  $context->tag = 'Solr';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'devtrac_solr_view_answers' => 'devtrac_solr_view_answers',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views_modes-modes' => array(
          'module' => 'views_modes',
          'delta' => 'modes',
          'region' => 'header',
          'weight' => '-10',
        ),
        'devtrac7_realms-5UTq3VgIQ00k1YbXRx4AbBDtoYTrI5J3' => array(
          'module' => 'devtrac7_realms',
          'delta' => '5UTq3VgIQ00k1YbXRx4AbBDtoYTrI5J3',
          'region' => 'content',
          'weight' => '-10',
        ),
        'facetapi-dzeTlysR6V8URCGL3z61h9euU2su6PwK' => array(
          'module' => 'facetapi',
          'delta' => 'dzeTlysR6V8URCGL3z61h9euU2su6PwK',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'facetapi-OfXUzvVxP7aNYIVbx2GFb4gOePGANblo' => array(
          'module' => 'facetapi',
          'delta' => 'OfXUzvVxP7aNYIVbx2GFb4gOePGANblo',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Active on all pages,');
  t('Solr');
  $export['solr_all_pages'] = $context;

  return $export;
}
