<?php
/**
 * @file
 * my_question_sms_feature.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function my_question_sms_feature_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_track_archive_dir';
  $strongarm->value = '4';
  $export['sms_track_archive_dir'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_track_archive_max_age_days';
  $strongarm->value = '0';
  $export['sms_track_archive_max_age_days'] = $strongarm;

  return $export;
}
