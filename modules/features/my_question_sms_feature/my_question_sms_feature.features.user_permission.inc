<?php
/**
 * @file
 * my_question_sms_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function my_question_sms_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'edit own sms number'.
  $permissions['edit own sms number'] = array(
    'name' => 'edit own sms number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'sms_user',
  );

  // Exported permission: 'receive sms'.
  $permissions['receive sms'] = array(
    'name' => 'receive sms',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'sms_user',
  );

  return $permissions;
}
